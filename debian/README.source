===============================
Debian packaging of calligra
===============================

This file aims to document the packaging of calligra for Debian. Please feel
free to improve it if necessary.

This documents only the bits specific to this source; general instructions
on updating a package version, uploading, etc are not described here.

Man pages
==========

If there is any demand, please ask upstream to provide them. Please do not add
Debian-specific ones, as they will rot easily, and be only in English.

Particularities
================

- Unit test files do not have a license [#]_;

.. [#] From upstream, may 2013:
 “At one point, I think we had the explicit policy that unittests weren't
 going to have a license header. I don't know why... That was back in the
 KOffice days.”

Embedded libs
==============

Calligra forks some library to fit to their purpose.

- filters/words/msword-odf/wv2 [#]_

.. [#] From upstream, may 2013:
 “The wv2 in Calligra is different from the one on sourceforge (which is
 pretty much dead and no longer developed), that's why wv2 in
 filters/words/msword-odf creates a library called kowv2.
 You cannot build Calligra against the sourceforge wv2.”

Missing libs
=============

Calligra could optionally use some libraries that are not yet in Debian.
I am listing them here, not to forget the day they will be in Debian.

Cauchy's M2MML
----------------

Required for the matlab/octave formula tool

https://bitbucket.org/cyrille/cauchy

.. vim:set filetype=rst:
